export function add(a, b) { return a + b; }
export function sub(a, b) { return a - b; }
export function mlt(a, b) { return a * b; }
export function div(a, b) { return a / b; }
export function sqr(a) { return mlt(a, a); }
