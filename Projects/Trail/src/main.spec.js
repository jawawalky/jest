import { add, div, mlt, sqr, sub } from './main';
import { test } from '@jest/globals';

test(
    '1 + 2 = 3',
    () => expect(add(1, 2)).toBe(3)
);

test(
    '3 - 4 = -1',
    () => expect(sub(3, 4)).toBe(-1)
);

test(
    '5 * 6 = 30',
    () => expect(mlt(5, 6)).toBe(30)
);

test(
    '8 / 2 = 4',
    () => expect(div(8, 2)).toBe(4)
);

test(
    'sqr(3) = 9',
    () => expect(sqr(3)).toBe(9)
);
