# Exercise
In this exercise we want to write our first test.

## Writing Test
1. Open the file [main.spec.js](../Projects/Trail/src/main.spec.js)
1. Write simple tests for all functions defined in [main.js](../Projects/Trail/src/main.js).
