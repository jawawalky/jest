# What You Will Learn About
In this section you will learn about

- how to add *Jest* to your project
- writing a simple test with *Jest*

# Jest
## What is Jest?
*Jest* is a *JavaScript* testing framework, which can be used in any
*JavaScript* project. It provides a simple and intuitive syntax for writing
tests, which allow us to validate the results of operations or the contents
of variables.

## Installation
We can add *Jest* to our project in the following way

### npm

```console
npm install --save-dev jest
```

### Yarn

```console
yarn add --dev jest
```

## ES6 Support
By default *Jest* uses *CommonJS* modules. Imports are then made by

```js
const add = require('./add');
```

If we want to use *ES6* modules, then we need to modify `package.json`
by adding

```json
{
	"type": "module",
	...
}
```

to the project configuration.

## Global Use of Jest
If we want to run *Jest* from the command line, then we need to install
*Jest* globally. That's done by

### npm

```console
npm install --global jest
```

### Yarn

```console
yarn global add jest
```

## Configuration
Further configurations of *Jest* can be made in a file called
`jest.config.mjs`.

When we run

```console
jest --init
```

in our project folder, then *Jest* will ask for some feature, we would like
to use. Based on our answers it creates the `jest.config.mjs` configuration
file.

## First Unit Test
Unit tests usually execute some code and then validate the outcome.

**Example**

```js
export function sayHello() { return 'Hello'; }
```

Now let's write some test for `sayHello()`.

If `sayHello()` was defined in a file called `hello.js`, then we would create
a file called `hello.spec.js`. We would write a test in the following way

**Example**

```js
import { sayHello } from './hello';
import { test } from '@jest/globals';

test(
    'Says Hello',
    () => expect(sayHello()).toBe('Hello')
);
```

A test starts with a short description, here `Says Hello` and is followed
by the test code. The test code is a function, which runs the code that we
want to test and it validates the results. `expect(...)` contains the code
to be tested and then offers *matchers*, here `toBe(...)` to validate
the result.

## Running Tests
If we are using *CommonJS* modules, then we can run the tests of our project
by 

```json
{
	...
	"scripts": {
    	...,
    	"test": "jest"
  	},
	...
}
```

If we use *ES6* modules, then we replace it by

```json
{
	...
	"type": "module",
	"scripts": {
    	...,
    	"test": "node --experimental-vm-modules node_modules/jest/bin/jest.js"
  	},
	...
}
```

Now we can run the tests with

```console
npm test
```

*Jest* should produce an output similar to this

```console
(node:64802) ExperimentalWarning: VM Modules is an experimental feature. This feature could change at any time
(Use `node --trace-warnings ...` to show where the warning was created)
 PASS  src/hello.spec.js
  ✓ Says Hello (2 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        0.21 s
Ran all test suites.
```
